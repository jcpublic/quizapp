# Quiz App
---

This repo is a complete example of how to do the Quiz App from class.
Full code given for both IOS and Android mobile apps.

## System requirements:

* Android: Java 8 + Android 7.0 Nougat
* IOS:  XCode 9+

## How to use

1. Download source code
2. Choose either Android or IOS
3. For Android projects, open directly in `Android Studio`
4. For IOS projects, open the `.xcworkspace` folder in `XCode`

## Connecting to Photon

In the code, add your Particle

* username
* password
* device_id

Press `PLAY`

Check the `Terminal`.  App will show success message if it was able to connect to the Particle Cloud.
